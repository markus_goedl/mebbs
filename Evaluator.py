# coding: utf-8

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import json
import numpy as np

print("Eval 1")
#1 reward threshold
red_patch = mpatches.Patch(color='red', label='reward/rate')
plt.legend(handles=[red_patch])
plt.xlabel('contribution_rate', fontsize=18)
plt.ylabel(u'reward(\u20ac/h)', fontsize=16)
plt.plot([0,2,1,3.5,4.5,5.5,6.5,7.5,10], [0,0,0,0.001,0.003,0.005,0.007,0.009,0.0105], 'ro')
plt.show()

red_patch = mpatches.Patch(color='blue', label='reward/rate')
plt.legend(handles=[red_patch])
plt.xlabel('contribution_rate', fontsize=18)
plt.ylabel('reward(MEBBS/h)', fontsize=16)
plt.plot([0,2,1,3.5,4.5,5.5,6.5,7.5,10], [0,0,0,0.1,0.3,0.5,0.7,0.9,1], 'bs')
plt.show()

red_patch = mpatches.Patch(color='green', label='reward/rate')
plt.legend(handles=[red_patch])
plt.xlabel('contribution_rate', fontsize=18)
plt.ylabel('reward(MEBBS)', fontsize=16)
plt.plot([0,2,1,3.5,4.5,5.5,6.5,7.5,10], [0,0,0,0.1,0.3,0.5,0.7,0.9,1], 'g^')
plt.show()

print("Eval 2")
#2 Failed packages
clients_rewards = []
clients_rewards_p = []
i = 1
while i < 11:
    with open("simulation_data/simulation_data_client"+str(i)+".txt") as simfile:
        client_rewards = []
        daily_reward = []
        days = 24
        for jsonobject in json.load(simfile):
            daily_reward.append(float(jsonobject['tx_failed']))
            days = days - 1
            if(days == 0):
                client_rewards.append(np.average(daily_reward))
                daily_reward = []
                days= 24
        clients_rewards.append(client_rewards)
        i = i + 1

for client_rew in clients_rewards:
    plt.plot(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,client_rew, 'b')
patch = mpatches.Patch(color='blue', label='packages failed/day')
plt.legend(handles=[patch])
plt.xlabel('days', fontsize=18)
plt.ylabel('packages failed', fontsize=16)
plt.show()

print("Eval 3")
#3 user categorization average 
i = 0
basics = 0
contributions = 0
with open("simulation_data/simulation_data_client1.txt") as simfile:
        for jsonobject in json.load(simfile):
            token = jsonobject['token']
            if token == "0E-8":
                basics = basics + 1
            else: contributions = contributions + 1
                    
label_cat = ['Basics', '','Contributions']
frecuencies = [basics,i,contributions]

pos = np.arange(len(label_cat))
width = 1.0    

ax = plt.axes()
ax.set_xticks(pos)
ax.set_xticklabels(label_cat)

plt.bar(pos, frecuencies, width, color='b')
plt.ylabel('session', fontsize=16)
plt.show()

print("Eval 4")
#4 €/day/client
clients_rewards = []
clients_rewards_p = []
i = 1
while i < 11:
    with open("simulation_data/simulation_data_client"+str(i)+".txt") as simfile:
        client_rewards = []
        client_rewards_p = []
        daily_reward = 0
        daily_reward_p = 0
        days = 24
        for jsonobject in json.load(simfile):
            if(jsonobject['token'] != "0E-8"):
                    daily_reward = daily_reward + float(jsonobject['token'])
                    daily_reward_p = daily_reward_p + float(jsonobject['token'])
            days = days - 1
            if(days == 0):
                client_rewards.append(round(daily_reward,3))
                client_rewards_p.append(round((daily_reward_p/7.5*100),3))
                daily_reward = 0
                days= 24
        clients_rewards_p.append(client_rewards_p)
        clients_rewards.append(client_rewards)
        i = i + 1

for client_rew in clients_rewards:
    plt.axis([0, 30, 0, 0.2])
    plt.plot(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,client_rew, 'g')

patch = mpatches.Patch(color='green', label=u'\u20ac/day')
plt.legend(handles=[patch])
plt.xlabel('days',fontsize = 18)
plt.ylabel(u'\u20ac',fontsize = 18)
plt.show()

print("Eval 5")
#5 reward % /day /user


for client_rew in clients_rewards_p:
    plt.axis([0, 30, 0, 60])
    plt.plot(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,client_rew, 'r')
patch = mpatches.Patch(color='red', label='fee recovery/day')
plt.legend(handles=[patch])
plt.xlabel('days',fontsize = 18)
plt.ylabel('fee recover %', fontsize=16)
plt.show()


print("Eval 6")
#6 avarage usage_rate per client (histogram)
clients_rewards = []
clients = []
i = 1
while i < 11:
    with open("simulation_data/simulation_data_client"+str(i)+".txt") as simfile:
        client_usage_rate = 0
        count = 0
        clients.append(str(i))
        for jsonobject in json.load(simfile):
            if(jsonobject['sage_rate_weighted'] != "0E-8"):
                client_usage_rate = client_usage_rate + float(jsonobject['sage_rate_weighted'])
                count += 1
        average_usage_rate = client_usage_rate/count
        clients_rewards.append(average_usage_rate)
        i = i + 1

plt.title('Average usage rate')
plt.xlabel('clients', fontsize=18)
plt.ylabel('usage rate', fontsize=16)
plt.xticks(range(len(clients)), clients)
width = 1.0
plt.bar(range(len(clients)) , clients_rewards , width, color='g')
plt.show()


print("Eval 7")
#7 how much token did the users earn in a month
clients_rewards = []
clients = []
i = 1
while i < 11:
    with open("simulation_data/simulation_data_client"+str(i)+".txt") as simfile:
        client_rewards = 0
        clients.append(str(i))
        for jsonobject in json.load(simfile):
                if(jsonobject['token'] != "0E-8"):
                    client_rewards = client_rewards + float(jsonobject['token'])
        clients_rewards.append(client_rewards)
        i = i + 1

plt.title('Rewards for clients per month')
plt.xlabel('clients', fontsize=18)
plt.ylabel("Euro", fontsize=16)
plt.xticks(range(len(clients)), clients)
width = 1.0
plt.bar(range(len(clients)) , clients_rewards , width, color='g')
plt.show()

print("Eval 8")
#8 how much do the users have to pay the next month?
#--> monthly fee - tokens earned
clients_rewards = []
clients = []
i = 1
while i < 11:
    with open("simulation_data/simulation_data_client"+str(i)+".txt") as simfile:
        client_rewards = 0
        clients.append(str(i))
        for jsonobject in json.load(simfile):
            if(jsonobject['token'] != "0E-8"):
                client_rewards = client_rewards + float(jsonobject['token'])
        clients_rewards.append(10 - client_rewards)
        i = i + 1

plt.title('Payments for the following month')
plt.xlabel('clients', fontsize=18)
plt.ylabel('Euro', fontsize=16)
plt.xticks(range(len(clients)), clients)
width = 1.0
plt.bar(range(len(clients)) , clients_rewards , width, color='g')
plt.show()