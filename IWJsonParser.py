from random import randint
def parseIwdump(output):
    stations = {}
    mac = None
   
    for line in output:
        line = line.strip()
        if line.startswith("Station"):
            mac = line.split()[1]
            if mac not in stations:
                details = {}
                stations[mac] = details
        if "rx bytes" in line:
            details["rx_bytes"] = str(line.strip().split()[2])
            details["fw_bytes"] = str(randint(0,int(line.strip().split()[2])))
        if "rx packets" in line:
            details["rx_packets"] = str(line.strip().split()[2])
            details["fw_packets"] = str(randint(0,int(line.strip().split()[2])))
        if "tx bytes" in line:
            details["tx_bytes"] = str(line.strip().split()[2])
        if "rx packets" in line:
            details["rx_packets"] = str(line.strip().split()[2])
        if "tx failed" in line:
            details["tx_failed"] = str(line.strip().split()[2])
    
    return stations

    
print("Starting iw-traces parsing...")

index = 1
while index < 32:
    hour = 1
    while hour < 25:
        mainfolder= "iw-traces/"
        jsonfolder = mainfolder + "jsonparsed/"
        filename = "18-03-"+str(index)+" ("+str(hour)+")"
        output = open(mainfolder+filename+".iwdump", "r")
        parsed = parseIwdump(output)
        with open(jsonfolder+filename+".json", "w+") as newfile:
           newfile.write(str(parsed).replace('\'','\"'))
        hour = hour + 1
    index = index + 1
print("Parsing finished")       
