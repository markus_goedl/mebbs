from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
import logging
import json
import time
import os
import errno
from socket import error as socket_error

class Client:
    'client class to interact with blockchain'

    def __init__(self, rpc_ip="0.0.0.0", rpc_port=8002, rpc_user="multichainrpc",
                 rpc_password="G7hD9ZQZZcRiQ3jqUqMYzKYV2HeJVqVfneuKabTLPCdx",
                 network_traffic_folder="jsonparsed",
                 masterWalletAddress="14CSbFWmVsDbKvf12yEkRcQAgjANVdxB1PmCdo",
                 asset_name="client1",
		 mac_address="44:d9:e7:d4:02:ed",
                 starting_fee=7.5):
        self.rpc_ip = rpc_ip
        self.rpc_port = rpc_port
        self.rpc_user = rpc_user
        self.rpc_password = rpc_password
        self.network_traffic_folder = network_traffic_folder
        self.masterWalletAddress = masterWalletAddress
        self.asset_name = asset_name
        self.mac_address = mac_address
        self.starting_fee=starting_fee
        self.rpc_connect()
        self.interrupted = False
        self.unit_value = 0.0105
        self.max_reward = 7.5

    def rpc_connect(self):
        self.rpc_connection = AuthServiceProxy("http://%s:%s@%s:%s"%(self.rpc_user, self.rpc_password,
                                                                     self.rpc_ip, self.rpc_port))

    def write_to_blockchain(self, amount, data):
        retries = 5
        while True:
            try:
                logging.info("writing to blockchain\namount %s\ndata: %s"%(amount, data))
                asset = {self.asset_name : amount}
                self.rpc_connection.sendwithdata(self.masterWalletAddress, asset, to_hex_value(data))
            except socket_error as serr:
                if serr.errno != errno.ECONNREFUSED:
                    raise serr
                # connection was refused -> retry up to 5 times
                logging.info("Connection refused. Number of retries: " + str(retries))
                if retries == 0:
                    raise serr
                retries -= 1
                time.sleep(30)
                self.rpc_connection = AuthServiceProxy("http://%s:%s@%s:%s"%(self.rpc_user, self.rpc_password,
                                                                             self.rpc_ip, self.rpc_port))

    def get_asset(self):
        assets = self.rpc_connection.listassets(self.asset_name)
        return assets

    def get_total_balances(self):
        balance = self.rpc_connection.gettotalbalances(0)
        return balance

    def get_wallet_transactions(self, count=3):
        transactions = self.rpc_connection.listwallettransactions(count)
        return transactions

    def start(self, interval_sec=1):
        index = 1
        previous_traffic_data_json = None
        while index < 32:
            hour = 1
            while hour < 25 and not self.interrupted:
                filename = "18-03-"+str(index)+" ("+str(hour)+")"
                file = self.network_traffic_folder + "/" + filename + ".json"
                with open(file, 'r') as traffic_data:
                    logging.info("reading file " + file)
                    traffic_data_json = json.load(traffic_data).get(self.mac_address)
                    if traffic_data_json == None:
                        traffic_data_json = previous_traffic_data_json
                    previous_traffic_data_json = traffic_data_json
                    values_usage_rate_weighted_plus_token_earned = self.calc_token_amount(traffic_data_json)
                    usage_rate_weighted = values_usage_rate_weighted_plus_token_earned[0]
                    traffic_data_json["usage_rate_weighted"] = usage_rate_weighted
                    amount = values_usage_rate_weighted_plus_token_earned[1]
                    self.write_to_blockchain(amount, json.dumps(traffic_data_json))
                    #print "Wallet transactions:", self.get_wallet_transactions(), "\n"
                    logging.info("New balance: " + str(self.get_total_balances()) + "\n-------------------------------------")

                hour = hour + 1
                time.sleep(interval_sec)
            index = index + 1

        logging.info("All files read.\nEnd of simulation.\n")

    
    def signal_handler(self, signal, frame):
        self.interrupted = True

    def calc_token_amount(self, traffic_data):
        rx_bytes = int(traffic_data["rx_bytes"])
        fw_bytes = int(traffic_data["fw_bytes"])
        tx_bytes = int(traffic_data["tx_bytes"])
        print("rx", rx_bytes, "fw", fw_bytes, "tx", tx_bytes)
        
        tx_bytes_cleaned = (tx_bytes-fw_bytes)
        rx_bytes_cleaned = (rx_bytes-fw_bytes)
        usage_rate = 0.0
        if tx_bytes_cleaned > 0:
            usage_rate += tx_bytes_cleaned
        if rx_bytes_cleaned > 0:
            usage_rate += rx_bytes_cleaned
        usage_rate_weighed = 0.0
        if fw_bytes+usage_rate <> 0:
            usage_rate_weighed = ((fw_bytes*7) + (usage_rate*3))/(fw_bytes+usage_rate)

        if usage_rate_weighed<3.5:
            token_earned = 0 
        else:
            token_earned = (self.unit_value*usage_rate_weighed)/self.max_reward
        
        print("Usage_rate for this transaction ->", usage_rate_weighed)
        print("Token earned for this transaction ->",token_earned)
        values_usage_rate_weighted_plus_token_earned = [usage_rate_weighed, token_earned]
        return values_usage_rate_weighted_plus_token_earned

def to_hex_value(data):
    data_hex = (str(data)).encode('hex')
    return data_hex

def from_hex_value(data_hex):
    data = (data_hex.decode('hex'))
    return data

logging.basicConfig(level=logging.INFO)

#this is used for the docker environment
#my_client = Client(rpc_ip=(os.environ['RPC_IP']), rpc_port=(os.environ['RPC_PORT']),
#                   rpc_user=(os.environ['RPC_USER']), rpc_password=(os.environ['RPC_PASSWORD']),
#                   asset_name=(os.environ['ASSET']), mac_address=(os.environ['MAC_ADDRESS']),
#                   masterWalletAddress=(os.environ['WALLET_ADDRESS']))
#my_client.start(float(os.environ['SUBMISSION_SPEED']))

#set the correct parameters for the client, else the standart values are used
my_client = Client()
my_client.start(0.5)



