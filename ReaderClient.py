import client
import json

class ReaderClient(client.Client):
    
    def sort_by_time(self, elem):
        return elem.get("time")

    def read_from_chain(self, asset_name, walletAddress, number_of_blocks):
        asset_data = self.rpc_connection.listassettransactions(asset_name, False, number_of_blocks)
        print "Number of blocks in asset " + asset_name + ": "+ str(len(asset_data))
        block_list = []
        for block in asset_data:
            data_hex = block.get("data")
            print data_hex
            if data_hex != []:
                data = client.from_hex_value(data_hex[0])
                print data
                data_json = json.loads(data)
                new_block = {}
                new_block["rx_bytes"] = data_json["rx_bytes"]
                new_block["tx_bytes"] = data_json["tx_bytes"]
                new_block["fw_bytes"] = data_json["fw_bytes"]
                new_block["tx_failed"] = data_json["tx_failed"]
                new_block["usage_rate_weighted"] = data_json["usage_rate_weighted"]
                new_block["token"] = block.get("addresses").get(walletAddress)
                new_block["time"] = block.get("time")
                new_block["block_index"] = str(block.get("blockindex"))
                block_list.append(new_block)
        block_list.sort(key = self.sort_by_time)
        return block_list

    def write_to_file(self, filepath, filename, block_list):
#very special customized parser ;)
        with open(filepath + filename, "w+") as new_file:
            new_file.write("[")
            lenght = len(block_list)
            i = 0
            while i < lenght - 1:
                block = block_list[i]
                block = str(block).replace('Decimal','')
                block = str(block).replace('u','')
                block = str(block).replace('(','')
                block = str(block).replace(')','')
                new_file.write(str(block).replace('\'','\"') + ",")
                i = i + 1
            block = block_list[i]
            block = str(block).replace('Decimal','')
            block = str(block).replace('u','')
            block = str(block).replace('(','')
            block = str(block).replace(')','')
            new_file.write(str(block).replace('\'','\"'))
            new_file.write("]")



def to_hex_value(data):
    data_hex = (str(data)).encode('hex')
    return data_hex

def from_hex_value(data_hex):
    data = (data_hex.decode('hex'))
    return data

mac_addresses = ["44:d9:e7:d4:02:ed", "04:18:d6:2a:17:83", "80:2a:a8:30:d9:06", "80:2a:a8:2a:68:b9",
                 "d4:ca:6d:11:31:8d", "00:15:6d:7c:d5:94", "98:de:d0:88:61:ec", "80:2a:a8:ee:b1:d7",
                 "00:27:22:8a:b8:28", "24:a4:3c:8a:9a:57"]
#mac_addresses = [ "00:27:22:8a:b8:28"]


for i in range(0, len(mac_addresses)):
    my_client = ReaderClient(mac_address=mac_addresses[i], asset_name="client" + str(i+1))
    my_client.start(0)
    print "Start reading from asset " + str(i),
    block_list = my_client.read_from_chain(my_client.asset_name, my_client.masterWalletAddress, 720)
    my_client.write_to_file("simulation_data/", "simulation_data_" + my_client.asset_name + ".txt", block_list)
#print str(block_list)
print "Finished reading asset"
